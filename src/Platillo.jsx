import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Platillo extends Component{
	constructor (props){
		super(props);
		this.state = {
			edit: 0,
		};
        this.edit = this.edit.bind(this);
        this.cancel = this.cancel.bind(this);
        this.save = this.save.bind(this);
        this.remove = this.remove.bind(this);
	}

	showEditingView(){
        return (  
            <div className="comida">
            <input ref={(editarComida) => this.editarComida = editarComida} type="text" className="form-control" placeholder="Nuevo nombre..." defaultValue={this.props.nombre}/>
                <div>
                    <div className="glyphicon glyphicon-ok-circle blue" onClick={this.save} />
                    <div className="glyphicon glyphicon-remove-circle red"  onClick={this.cancel}/>
                </div>
            </div>
            );
    }

	edit(){
        this.setState({edit: 1});	
    }
   
    save(){
        this.props.onChange.call(null,this.editarComida.value, this.props.index);
        this.setState({edit: 0});
    }

	remove() {
        this.props.onRemove.call(null,this.props.index);
    }

    cancel(){
        this.setState({edit: 0})
    }

    showFinalView(){
    	return(
			<div className="comida">
                <h1 className="bg-success">{this.props.nombre}</h1>
                <p className="bg-info">
                    Posición: <i>{this.props.index + 1}</i>
                </p>
                <div>
                    <input type="checkbox" className="glyphicon glyphicon-heart heart" 
                    />
                    <br />
                    Like: 
                </div>
                <div>
                    <div className="glyphicon glyphicon-pencil blue" onClick={this.edit}/>
                    <div className="glyphicon glyphicon-trash red" onClick={this.remove}/>
                </div>
            </div>
		);
    }

	render(){
		 if (this.state.edit === 1) {
            return this.showEditingView();
        } else {
            return this.showFinalView();
        }
	}
}

Platillo.propTypes ={
	nombre : PropTypes.string.isRequired,
	index: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
};

export default Platillo;