import React, { Component } from 'react';
import Platillo from './Platillo.jsx';
import PropTypes from 'prop-types';
import './App.css';


class Comida extends Component{

	constructor (props){
		super(props);
        this.eachItem = this.eachItem.bind(this);
		this.add = this.add.bind(this);
        this.update = this.update.bind(this);
        this.remove = this.remove.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
	}  

	add() {
	    var nuevaComida = this.NuevaComida.value;
	    if (nuevaComida === "")
        {
            if(typeof(nuevaComida) !== 'undefined')
            {
               nuevaComida = "Nueva comida";
            }
        }
         var arr = this.props.lista;
        arr.push(nuevaComida);
        this.setState({lista: arr});
	 }

     update(nuevoNombre, i) {
        var arr = this.props.lista;
        arr[i] = nuevoNombre;
        this.setState({lista:arr});
    }

    remove(i){
        var arr = this.props.lista;
        arr.splice(i, 1);
        this.setState({lista:arr});
    }

    handleKeyDown(e){
        if( e.charCode === 13 ) {
            this.add();
        }
    }

	eachItem(comida, i) {
        return (
                <Platillo key={i}
                    index={i}
                    nombre={comida} 
                    onChange={this.update}
                    onRemove={this.remove}
                    >
                    {i+1}
                </Platillo>
            );
    }

	render(){
		return(
			<div className="centerBlock">
                <header>
                    <h1>Mis comidas favoritas</h1>
                    <i>Total: {this.props.lista.length}</i>
                    <br />
                    <span className="glyphicon glyphicon-refresh"></span>
                </header>
                <div className="input-group">
                    <input ref={(NuevaComida) => this.NuevaComida = NuevaComida} onKeyPress={this.handleKeyDown} type="text" className="form-control" placeholder="Agregar nueva comida..." />
                    <span className="input-group-btn">
                        <div className="btn btn-default btn-success" type="button" onClick={this.add}> + </div>
                    </span>
                </div>
                <div>
                	{this.props.lista.map(this.eachItem)}
                </div>
            </div>
			);
	}

}

Comida.propTypes ={
	lista : PropTypes.array.isRequired,
};

export default Comida;
