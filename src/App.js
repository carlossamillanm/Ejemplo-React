import React, { Component } from 'react';
import Comida from './Comida.jsx';
import './Bootstrap/css/bootstrap.min.css'
import './App.css';

class App extends Component {

constructor (props){
	super(props);
	this.state ={
		comidas: [
                'Tacos',
                'Paella',
                'Ceviche',
        ],
	};
}

  render() {
    return (
      <div className="App">
      	<Comida lista={this.state.comidas}/>
      </div>
    );
  }
}

export default App;
